#pragma once

#include "../../cats-framework/include/util/SubsetInt.hpp"
#include "../../cats-framework/include/SearchManager.hpp"

#include "NodeFlowshop.hpp"
#include "Instance.hpp"
#include "checker.hpp"

using namespace cats;

enum StratMakespan2 {
    MAKESPAN_2,
    IDLE_TIME_2,
    ALPHA_2,
    W_ALPHA_2
};

/**
 * bidirectionnal search for the flowshop (makespan minimization)
 * implements various guidance strategies
 */
class NodeMakespan2 : public NodeFlowshop {
 private:
    Instance& inst_;
    SearchManager& manager_;
    std::vector<JobId> prefix_forw_;
    std::vector<JobId> prefix_back_;
    std::vector<ProcessingTime> front_forw_;
    std::vector<ProcessingTime> front_back_;
    std::vector<ProcessingTime> remaining_p_;  // remaining processing times on all machines
    ProcessingTime total_idle_;
    ProcessingTime makespan_bound_;
    SubsetInt added_subset_;
    std::vector<ProcessingTime> idle_forw_;
    std::vector<ProcessingTime> idle_back_;
    StratMakespan2 guide_;

 public:
    explicit NodeMakespan2(Instance& inst, SearchManager& manager, StratMakespan2 guide): NodeFlowshop(),
        inst_(inst), manager_(manager), total_idle_(0), makespan_bound_(0), guide_(guide) {
            for (MachineId m = 0; m < inst_.getNbMachines(); m++) {
                front_forw_.push_back(0);
                front_back_.push_back(0);
                idle_forw_.push_back(0);
                idle_back_.push_back(0);
                remaining_p_.push_back(inst_.getSumProcessingTimes(m));
            }
        }

    explicit NodeMakespan2(const NodeMakespan2& s): NodeFlowshop(s),
        inst_(s.inst_), manager_(s.manager_), 
        prefix_forw_(s.prefix_forw_), prefix_back_(s.prefix_back_), front_forw_(s.front_forw_), front_back_(s.front_back_), 
        remaining_p_(s.remaining_p_), total_idle_(s.total_idle_), makespan_bound_(s.makespan_bound_), added_subset_(s.added_subset_), 
        idle_forw_(s.idle_forw_), idle_back_(s.idle_back_), guide_(s.guide_) {}

    NodePtr copy() const override { return NodePtr(new NodeMakespan2(*this)); }


    inline double evaluate() const override {
        return makespan_bound_;
    }

    inline double guide() override {
        if ( guide_ == MAKESPAN_2 ) {
            return makespan_bound_;
        } else if ( guide_ == IDLE_TIME_2 ) {
            return total_idle_;
        } else if ( guide_ == ALPHA_2 ) {
            double alpha = ((double) prefix_forw_.size()+prefix_back_.size())/inst_.getNbJobs();
            return makespan_bound_*alpha + total_idle_*(1.-alpha);
        } else if ( guide_ == W_ALPHA_2 ) {  // Weighted Alpha
            double alpha = ((double) prefix_forw_.size()+prefix_back_.size())/inst_.getNbJobs();
            double val = 0;
            MachineId last = inst_.getNbMachines()-1;
            for (MachineId m = 1; m < inst_.getNbMachines(); m++) {
                val += (double) idle_forw_[m]/front_forw_[m];
            }
            for (int m = last-1; m >= 0; m--) {
                val += (double) idle_back_[m]/front_back_[m];
            }
            val = val*makespan_bound_;
            return makespan_bound_*(alpha) + val*(1-alpha);
        } else {
            assert(false);
        }
        return 0;
    }

    std::vector<NodePtr> getChildren() override {
        std::vector<NodePtr> forw_children;
        std::vector<NodePtr> back_children;
        int nb_pruned_forward = 0;
        int nb_pruned_backward = 0;
        for (JobId j = 0; j < inst_.getNbJobs(); j++) {
            if (!added_subset_.contains(j)) {
                // Prefix
                NodeMakespan2* child = new NodeMakespan2(*this);
                child->add_job_prefix(j);
                NodePtr tmp = NodePtr(child);
                if (!manager_.isDominatedByBest(tmp)) {
                    forw_children.push_back(tmp);
                } else {
                    nb_pruned_forward++;
                }
                // Suffix
                child = new NodeMakespan2(*this);
                child->add_job_suffix(j);
                tmp = NodePtr(child);
                if (!manager_.isDominatedByBest(tmp)) {
                    back_children.push_back(tmp);
                } else {
                    nb_pruned_backward++;
                }
            }
        }
        // choose the one with the smaller children number
        if (back_children.size() < forw_children.size()) {
            for ( int i = 0 ; i < nb_pruned_backward ; i++ ) manager_.getSearchStats().add_nb_pruned();
            return back_children;
        } else if (back_children.size() > forw_children.size()) {
            for ( int i = 0 ; i < nb_pruned_forward ; i++ ) manager_.getSearchStats().add_nb_pruned();
            return forw_children;
        } else { // tie breaking: choose the one with the greater bound sum
            ProcessingTime sum_f = 0;
            ProcessingTime sum_b = 0;
            for (JobId j = 0; j < forw_children.size(); j++) {
                sum_f += forw_children[j]->evaluate();
                sum_b += back_children[j]->evaluate();
            }
            if (sum_f > sum_b) {
                for ( int i = 0 ; i < nb_pruned_forward ; i++ ) manager_.getSearchStats().add_nb_pruned();
                return forw_children;
            } else {
                for ( int i = 0 ; i < nb_pruned_backward ; i++ ) manager_.getSearchStats().add_nb_pruned();
                return back_children;
            }
        }
    }

    NodePtr getOriginalBS() override {
        return copy();
    }

    inline bool isGoal() const override {
        return static_cast<JobId>(getsol().size()) == inst_.getNbJobs();
    }

    /**
     * \brief called when a new best solution is found. Checks that the solution is feasible. If not, explain why
     */
    void handleNewBest() override {
        std::vector<JobId> res = getsol();
        assert(checker(inst_,res, obj_) == this->evaluate());
    }

    std::vector<JobId> getsol() const override{
        std::vector<JobId> res = std::vector<JobId>();
        for (auto e : prefix_forw_) {
            res.push_back(e);
        }
        for (int i = prefix_back_.size()-1 ; i >= 0 ; i--) {
            res.push_back(prefix_back_[i]);
        }
        return res;
    }

    std::string getName() override {
        if ( guide_ == MAKESPAN_2 ) {
            return "FB[Makespan;bound]";
        } else if ( guide_ == IDLE_TIME_2 ) {
            return "FB[Makespan;idle]";
        } else if ( guide_ == ALPHA_2 ) {
            return "FB[Makespan;alpha]";
        } else if ( guide_ == W_ALPHA_2 ) {
            return "FB[Makespan;Walpha]";
        } else {
            assert(false);
        }
        return "?";
    }

    void add_job_prefix(JobId job) {
        // Update front
        front_forw_[0] += inst_.getDuration(0, job);
        for (MachineId m = 1; m < inst_.getNbMachines(); m++) {
            ProcessingTime start;
            if (front_forw_[m-1] > front_forw_[m]) {
                start = front_forw_[m-1];
                total_idle_ += front_forw_[m-1] - front_forw_[m];
                idle_forw_[m] += front_forw_[m-1] - front_forw_[m];
            } else {
                start = front_forw_[m];
            }
            front_forw_[m] = start + inst_.getDuration(m, job);
        }
        prefix_forw_.push_back(job);
        added_subset_.add(job);
        // update bound
        for (MachineId m = 0; m < inst_.getNbMachines(); m++) {
            remaining_p_[m] -= inst_.getDuration(m, job);
            makespan_bound_ = std::max(
                makespan_bound_,
                static_cast<ProcessingTime>(front_forw_[m] + remaining_p_[m] + front_back_[m])
            );
        }
    }

    void add_job_suffix(JobId job) {
        // Update front
        MachineId last = inst_.getNbMachines()-1;
        front_back_[last] += inst_.getDuration(last, job);
        for (int m = last-1; m >= 0; m--) {
            ProcessingTime start;
            if (front_back_[m+1] > front_back_[m]) {
                start = front_back_[m+1];
                total_idle_ += front_back_[m+1] - front_back_[m];
                idle_back_[m] += front_back_[m+1] - front_back_[m];
            } else {
                start = front_back_[m];
            }
            front_back_[m] = start + inst_.getDuration(m, job);
        }
        prefix_back_.push_back(job);
        added_subset_.add(job);
        // update bound
        for (MachineId m = 0; m < inst_.getNbMachines(); m++) {
            remaining_p_[m] -= inst_.getDuration(m, job);
            makespan_bound_ = std::max(
                makespan_bound_,
                static_cast<ProcessingTime>(front_forw_[m] + remaining_p_[m] + front_back_[m])
            );
        }
    }
};