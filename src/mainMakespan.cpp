/**
 * runs tree search algorithms to solve the permutation flowshop problem (makespan minimization)
 */

#include <iostream>
#include <omp.h>
#include <signal.h>

#include "../../cats-framework/include/util/includeAlgorithms.hpp"
#include "../../cats-framework/include/SearchManager.hpp"
#include "../../cats-framework/include/combinators/StatsCombinator.hpp"

#include "Instance.hpp"
#include "NodeMakespan1.hpp"  // forward search (bound, idleTime, alpha)
#include "NodeMakespan2.hpp"  // bidirectional search (wAlpha)
#include "NodeMakespan4.hpp"  // forward search (wAlpha)

namespace pfsp_makespan {

enum Method {
    Makespan1,
    Makespan2,
    Makespan3,
    Makespan4
};

}  // namespace pfsp_makespan

using namespace cats;
using namespace pfsp_makespan;


SearchManager search_manager;
std::string output_filename = "out.sol";
std::string json_filename = "out.json";

/**
 * print statistics at the end of the search
 */
void handle_end_search() {
    std::cout << "=== SEARCH ENDED ===" << std::endl;
    search_manager.printStats();

    search_manager.writeAnytimeCurve(json_filename, "IB");
    if (!output_filename.empty()){
        search_manager.writeSolution(output_filename);
    }
    exit(0);
}



int main(int argc, char* argv[]) {
    if ( argc < 5 ) {
        std::cout << "\n[ERROR] USAGE: " << argv[0] << " INSTANCE TIME VARIANT GUIDE [OUTPUT_SOL] [OUTPUT_JSON]" << std::endl;
        return 1;
    }

    // parse user input
    std::cout << argv[1] << std::endl;
    Instance inst(argv[1]);
    float time_limit = std::stof(argv[2]);
    Method method = static_cast<Method>(std::stoi(argv[3]));
    int guide = static_cast<int>(std::stoi(argv[4]));
    if ( argc >= 6 ) {
        output_filename = argv[5];
    }
    if ( argc >= 7 ) {
        json_filename = argv[6];
    }

    // print instance
    inst.print();    

    /** create event handlers */
    signal(SIGINT, [](int signal) {
        fprintf(stderr, "Error user kill: signal %d:\n", signal);
        handle_end_search();
        exit(1);
    });

    // start search manager
    search_manager.start();

    // define root of the tree
    
    NodePtr root_ptr;
    
    if ( method == Makespan1 ) {
        root_ptr = NodePtr(new NodeMakespan1(inst, search_manager, static_cast<StratMakespan1>(guide)));
    } else if ( method == Makespan2 ) {
        root_ptr = NodePtr(new NodeMakespan2(inst, search_manager, static_cast<StratMakespan2>(guide)));
    } else if ( method == Makespan4 ) {
        root_ptr = NodePtr(new NodeMakespan4(inst, search_manager));
    }


    root_ptr = NodePtr(new StatsCombinator<>(root_ptr, search_manager, search_manager.getSearchStats(), false));

    TreeSearchParameters ts_params = {.root = root_ptr, .manager = search_manager, .id = 0};
    IterativeBeamSearch al = IterativeBeamSearch(ts_params, 1, 2, true);

    al.run(time_limit);

    handle_end_search();
    
    return 0;
}
