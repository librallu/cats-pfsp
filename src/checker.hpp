#pragma once

#include <algorithm>

#include "Instance.hpp"


double checker(Instance& inst, std::vector<JobId> sol, Objective obj) {
    // Check that sol contains all jobs 1 time
    for (JobId n = 0; n < inst.getNbJobs(); n++) {
        if (std::count(sol.begin(), sol.end(), n) != 1) {
            std::cout << "job " << n << " appears " << std::count(sol.begin(), sol.end(), n) << " times." << std::endl;
            return -1;
        }
    }

    if (obj == Makespan){
        // Compute objective value
        std::vector<ProcessingTime> front_(inst.getNbMachines(), 0);
        for (JobId j : sol) {
            front_[0] += inst.getDuration(0, j);
            for (MachineId m = 1; m < inst.getNbMachines(); m++) {
                ProcessingTime start = std::max(front_[m-1], front_[m]);
                front_[m] = start + inst.getDuration(m, j);
            }
        }
        return front_.back();
    }else if (obj == Flowtime){
        // Compute objective value
        ProcessingTime flowtime = 0;
        std::vector<ProcessingTime> front_(inst.getNbMachines(), 0);
        for (JobId j : sol) {
            front_[0] += inst.getDuration(0, j);
            for (MachineId m = 1; m < inst.getNbMachines(); m++) {
                ProcessingTime start = std::max(front_[m-1], front_[m]);
                front_[m] = start + inst.getDuration(m, j);
            }
            flowtime += front_.back();
        }
        return flowtime;
    }else{
        return 0; //not implemented
    }
}

