/**
 * runs tree search algorithms to solve the permutation flowshop problem (flowtime minimization)
 */

#include <iostream>
#include <omp.h>
#include <signal.h>

#include "../../cats-framework/include/util/includeAlgorithms.hpp"
#include "../../cats-framework/include/SearchManager.hpp"
#include "../../cats-framework/include/combinators/StatsCombinator.hpp"

#include "Instance.hpp"
#include "NodeFlowtime1.hpp"
#include "NodeFlowtime2.hpp"

using namespace cats;

enum Method {
    Flowtime1,
    Flowtime2
};


SearchManager search_manager;
std::string output_filename = "out.sol";
std::string json_filename = "out.json";

/**
 * print statistics at the end of the search
 */
void handle_end_search() {
    std::cout << "=== SEARCH ENDED ===" << std::endl;
    search_manager.printStats();

    search_manager.writeAnytimeCurve(json_filename, "IB");
    if (!output_filename.empty()){
        search_manager.writeSolution(output_filename);
    }
    exit(1);
}



int main(int argc, char* argv[]) {
    if ( argc < 4 ) {
        std::cout << "\n[ERROR] USAGE: " << argv[0] << " INSTANCE TIME VARIANT [OUTPUT_SOL] [OUTPUT_JSON]" << std::endl;
        return 1;
    }

    // parse user input
    Instance inst(argv[1]);
    float time_limit = std::stof(argv[2]);
    Method method = static_cast<Method>(std::stoi(argv[3]));
    if ( argc >= 5 ) {
        output_filename = argv[4];
    }
    if ( argc >= 6 ) {
        json_filename = argv[5];
    }

    // print instance
    inst.print();    

    /** create event handlers */
    signal(SIGINT, [](int signal) {
        fprintf(stderr, "Error user kill: signal %d:\n", signal);
        handle_end_search();
        exit(1);
    });

    // start search manager
    search_manager.start();

    // define root of the tree
    
    NodePtr root_ptr;
    
    if ( method == Flowtime1 ) {
        root_ptr = NodePtr(new NodeFlowtime1(inst, search_manager));
    } else if (method == Flowtime2 ) {
        root_ptr = NodePtr(new NodeFlowtime2(inst, search_manager));
    }

    root_ptr = NodePtr(new StatsCombinator<>(root_ptr, search_manager, search_manager.getSearchStats(), false));
    // root_ptr = NodePtr(new BucketListCombinator<PE_flowshop>(root_ptr, search_manager, pe_store));

    TreeSearchParameters ts_params = {.root = root_ptr, .manager = search_manager, .id = 0};
    IterativeBeamSearch al = IterativeBeamSearch(ts_params, 1, 2, true);
    al.run(time_limit);

    handle_end_search();
    
    return 0;
}
