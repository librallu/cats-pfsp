#include "Instance.hpp"
#include "checker.hpp"



int main(int argc, char* argv[]) {
    if ( argc < 4 ) {
        std::cout << "\n[ERROR] USAGE: " << argv[0] << " INSTANCE_NAME SOLUTION_FILENAME OBJECTIVE" << std::endl;
        std::cout << "OBJECTIVE:\n";
        std::cout << "\t0: Makespan\n";
        std::cout << "\t1: Flowtime\n";
        return 1;
    }

    // parse user input
    Instance inst(argv[1]);

    // create solution
    std::vector<JobId> sol;
    std::ifstream f;
    f.open(argv[2]);
    if ( !f.is_open() ) {
        throw FileNotFoundException();
    }
    JobId job;
    while (f >> job) {
        sol.push_back(job);
    }
    f.close();

    Objective objective = static_cast<Objective>(std::stoi(argv[3]));

    // call checker
    double checker_result = checker(inst, sol, objective);
    if (checker_result < 0) {
        std::cout << "The solution is NOT valid." << std::endl;
        return -1;
    } else {
        std::cout << "Solution value: " << checker_result << std::endl;
        return 0;
    }
    
}
