#pragma once

#include <vector>
#include <memory>
#include <string>
#include <iostream>
#include <functional>
#include <algorithm>
#include <fstream>

#include "Instance.hpp"
#include "checker.hpp"
#include "../../cats-framework/include/Node.hpp"

namespace cats {


class NodeFlowshop : public Node {
 public:

    std::string solToStr() override{
        std::string sol = "";
        if (isGoal()){
            std::vector<JobId> res = getsol();
            for (JobId j: res){
                sol += std::to_string(j) + " ";
            }
        }else{
            fprintf(stderr, "Error no solution found:\n");
        }
        return sol;
    }

    virtual std::vector<JobId> getsol() const { std::vector<JobId> sol; return sol;}

};


}  // namespace cats
