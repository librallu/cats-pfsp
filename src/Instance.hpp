#pragma once

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>

#include "../../cats-framework/include/io.hpp"

using namespace cats;

enum Objective {
    Makespan,
    Flowtime
};

typedef uint16_t MachineId;
typedef uint16_t JobId;
typedef int32_t ProcessingTime;

class Instance {
 private:
    std::string name_;  ///< instance name
    std::vector<std::vector<ProcessingTime>> durations_;

    //// COMPUTED DATA
    std::vector<double> avg_processing_time_;
    std::vector<ProcessingTime> sum_processing_time_;

    //// SORTED JOBS
    std::vector<JobId> sorted_jobs_;

 public:
    explicit Instance(std::string filename) {
        name_ = filename;
        std::ifstream f;
        f.open(filename);
        if ( !f.is_open() ) {
            throw FileNotFoundException();
        }
        JobId n_;
        MachineId m_;
        f >> n_;
        f >> m_;

        // read processing times
        for (MachineId m = 0; m < m_; m++) {
            durations_.push_back(std::vector<ProcessingTime>());
            ProcessingTime p;
            for (JobId n = 0; n < n_; n++) {
                f >> p;
                durations_[m].push_back(p);
            }
        }
        f.close();

        // compute additional attributes
        populateComputedAttributes();

        // construct sorted jobs (decresing sum of processing times)
        populateSortedJobs();
    }

    explicit Instance(std::vector<std::vector<ProcessingTime>> durations) {
        // read processing times
        durations_ = durations;
        // compute additional attributes
        populateComputedAttributes();
        // construct sorted jobs (decresing sum of processing times)
        populateSortedJobs();
    }
    
    /**
     * Returns the number of machines in the instance
     */
    inline int getNbMachines() const { return durations_.size(); }

    /**
     * Returns the number of jobs in the instance
     */
    inline int getNbJobs() const {return durations_[0].size(); }


    /**
     * returns the duration of a job for a given machine and job.
     * Identifiers starts at 0
     */
    inline ProcessingTime getDuration(MachineId machine_id, JobId job_id) const {
        return durations_[machine_id][job_id];
    }

    void print() {
        std::cout << "## " << name_ << " ##\n";
        std::cout << "nb jobs\t" << getNbJobs() << std::endl;
        std::cout << "nb machines\t" << getNbMachines() << std::endl;
        // std::cout << "processing time :" << std::endl;
        // for (MachineId m = 0; m < getNbMachines(); m++) {
        //     for (JobId n = 0; n < getNbJobs(); n++) {
        //         std::cout << getDuration(m, n) << "\t";
        //     }
        //     std::cout << std::endl;
        // }
    }


    //////// COMPUTED ATTRIBUTE GETTERS
    inline double getAverageProcessingTime(MachineId m) const {
        return avg_processing_time_[m];
    }

    inline ProcessingTime getSumProcessingTimes(MachineId m) const { return sum_processing_time_[m]; }

    /**
     *  get job at position i in the sorted list
     */
    inline JobId getNthJob(JobId i) const { return sorted_jobs_[i]; }


 private:

    void populateComputedAttributes() {
        // compute average processing time
        avg_processing_time_ = std::vector<double>(getNbMachines(), 0.);
        sum_processing_time_ = std::vector<ProcessingTime>(getNbMachines(), 0);
        for ( MachineId m = 0 ; m < getNbMachines() ; m++ ) {
            for ( JobId j = 0 ; j < getNbJobs() ; j++ ) {
                avg_processing_time_[m] += getDuration(m, j);
                sum_processing_time_[m] += getDuration(m, j);
            }
            avg_processing_time_[m] /= getNbJobs();
        }
    }

    void populateSortedJobs() {
        std::vector<ProcessingTime> tmp = std::vector<ProcessingTime>(getNbJobs(), 0);
        for ( JobId i = 0 ; i < getNbJobs() ; i++ ) {
            sorted_jobs_.push_back(i);
            for ( MachineId m = 0 ; m < getNbMachines() ; m++ ) {
                tmp[i] += getDuration(m, i);
            }
        }
        std::sort(sorted_jobs_.begin(), sorted_jobs_.end(), [tmp](JobId i, JobId j){
            return tmp[i] > tmp[j];
        });
    }

};
