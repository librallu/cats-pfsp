#pragma once

#include <cmath>
#include <boost/functional/hash.hpp>

#include "../../cats-framework/include/util/SubsetInt.hpp"
#include "../../cats-framework/include/SearchManager.hpp"

#include "NodeFlowshop.hpp"
#include "Instance.hpp"
#include "checker.hpp"

using namespace cats;


/**
 * Forward search for the permutation flowshop (makespan minimization)
 * It uses a guide that uses the (not weighted) idle time close to the root and the makespan close to solution nodes.
 * Proposes the "bound, idle_time, alpha" as guidance strategies.
 */
class NodeMakespan4 : public NodeFlowshop {
 private:
    Instance& inst_;
    SearchManager& manager_;
    std::vector<JobId> prefix_;  ///< set of scheduled jobs in the prefix
    std::vector<ProcessingTime> front_forw_;  ///< position of the prefix front
    ProcessingTime total_idle_;  ///< total idle time on all machines
    SubsetInt added_subset_;  ///< used to quickly access the set of processed jobs
    std::vector<ProcessingTime> idle_machines_;  ///< idle times for each machine


 public:
    explicit NodeMakespan4(Instance& inst, SearchManager& manager): NodeFlowshop(), inst_(inst), manager_(manager), prefix_(), 
    front_forw_(0), total_idle_(0), added_subset_() {
        front_forw_ = std::vector<ProcessingTime>(inst_.getNbMachines(), 0);
        idle_machines_ = std::vector<ProcessingTime>(inst_.getNbMachines(), 0);
    }

    explicit NodeMakespan4(const NodeMakespan4& s): NodeFlowshop(s),
        inst_(s.inst_), manager_(s.manager_), prefix_(s.prefix_), front_forw_(s.front_forw_), total_idle_(s.total_idle_), 
        added_subset_(s.added_subset_), idle_machines_(s.idle_machines_) {}

    NodePtr copy() const override { return NodePtr(new NodeMakespan4(*this)); }

    inline double evaluate() const override {
        return front_forw_.back();
    }

    inline double guide() override {
        double res = 0.;
        for ( MachineId m = 1 ; m < inst_.getNbMachines() ; m++ ) {
            res += idle_machines_[m]/static_cast<double>(front_forw_[m]);
        }
        return res;
    }

    std::vector<NodePtr> getChildren() override {
        std::vector<NodePtr> res;
        for (JobId j = 0; j < inst_.getNbJobs(); j++) {
            if (!added_subset_.contains(j)) {
                NodeMakespan4* child = new NodeMakespan4(*this);
                child->add_job(j);
                NodePtr tmp = NodePtr(child);
                if (!manager_.isDominatedByBest(tmp)) {
                    res.push_back(tmp);
                } else {
                    manager_.getSearchStats().add_nb_pruned();
                }
            }
        }
        return res;
    }

    inline bool isGoal() const override {
        return static_cast<JobId>(getsol().size()) == inst_.getNbJobs();
    }

    /**
     * \brief called when a new best solution is found. Checks that the solution is feasible. If not, explain why
     */
    void handleNewBest() override {
        std::vector<JobId> res = getsol();
        assert(checker(inst_,res, obj_) == this->evaluate());
    }

    std::vector<JobId> getsol() const override {
        return prefix_;
    }

    std::string getName() override {
        return "Forw[Makespan;Walpha]";
    }

 private:
    inline void add_job(JobId job) {
        front_forw_[0] += inst_.getDuration(0, job);
        for (MachineId m = 1; m < inst_.getNbMachines(); m++) {
            ProcessingTime start;
            if (front_forw_[m-1] > front_forw_[m]) {
                double local_idle = front_forw_[m-1] - front_forw_[m];
                start = front_forw_[m-1];
                total_idle_ += local_idle;
                idle_machines_[m] += local_idle;
            } else {
                start = front_forw_[m];
            }
            front_forw_[m] = start + inst_.getDuration(m, job);
        }
        prefix_.push_back(job);
        added_subset_.add(job);
    }
};