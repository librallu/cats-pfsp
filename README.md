# cats-pfsp

CATS implementation of the Permutation Flowshop problem.
Code sources of experiments performed for "Iterative beam search algorithms for the permutation flowshop"

The present code uses the CATS framework (Combinator-based Anytime Tree Search)


## Installation and compilation

This project requires to be positioned next to [cats-framework](https://gitlab.com/librallu/cats-framework) and requires [CMake](https://cmake.org/).

 1. `mkdir ~/cats-dev ; cd ~/cats-dev`
 2. `git clone https://gitlab.com/librallu/cats-framework`
 3. `git clone https://gitlab.com/librallu/cats-pfsp` 
 4. `cd cats-pfsp/`
 5. `cmake . ; make`

## project structure

- **src/** search source files (C++)
- **numerical_results/** contains all solutions (.sol files) and search statistics (.json files) that include number of nodes / second opened, performance profiles and many more information.
- **insts/** contains all instances (large VFR and Taillard) instances converted to the same format.


## usage example

### checker.exe
Checks the solution value. checker.exe takes 3 parameters:
 - instance file
 - solution file (.sol)
 - objective (0 for makespan, 1 for flowtime)

```
./checker.exe insts/Large/VFR800_60_10_Gap.txt numerical_results/makespan/FB_g4/VFR800_60_10.sol 0
Solution value: 45383
```

### flowtime.exe

Solves a flowtime instance. Takes as parameters:
 - instance file
 - timelimit (in seconds)
 - variant (0 for Fg3, 1 for Fg4)
 - output solution (.sol file)
 - search information (.json file)

```
./flowtime.exe insts/Taillard/tai500_20_9.txt 30 1 output.sol output.json
```


### makespan.exe

Solves a makespan instance. Takes as parameters:
 - instance file
 - timelimit (in seconds)
 - variant (0 for forward, 1 for bidirectional)
 - guide (0 for g1, 1 for g2, 2 for g3, 3 for g4)
 - output solution (.sol file)
 - search information (.json file)

```
./makespan.exe insts/Large/VFR800_60_10_Gap.txt 30 1 3 out.sol out.json
```
